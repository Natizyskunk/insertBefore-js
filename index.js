document.body.onload = addElement;

function addElement () { 
	const currentApp 		= document.getElementById("app");
	const myHr 				= document.querySelectorAll("hr");
	var newDiv 		 		= document.createElement("div"); 
	var newParagraph 		= document.createElement('p'); 
	// var newHr 				= document.createElement("hr");
	var newBr 				= document.createElement("br");
	var newHello 			= document.createTextNode("Hello");
	var newFirstContent 	= document.createTextNode("Ceci est placé en premier");
	var newSecondContent	= document.createTextNode("Ceci est placé en second");
	
	currentApp.appendChild(newParagraph);
	newParagraph.appendChild(newHello);
	
	// newDiv.appendChild(newContent);
	
	var parentDiv = currentApp.parentNode;
	
	parentDiv.insertBefore(newFirstContent, currentApp);
	parentDiv.insertBefore(newBr, currentApp);
	parentDiv.insertBefore(newSecondContent, currentApp);
		
	var u = "";
	for (var i=0; i<5; i++) {
		u = u + "\n" + i;
	}
	console.log(u);
	
	// var insertedNode = parentNode.insertBefore(newNode, referenceNode);
	// document.body.insertBefore(newHr, newFirstContent);
	// currentApp.insertBefore(newHr, newFirstContent);
}

/* test();

function test () {
	// Crée un nouvel élément <span> simple
	var sp1 = document.createElement("span");

	// Obtient une référence à l'élément avant lequel nous voulons insérer
	var sp2 = document.getElementById("childElement");
	// Obtient une référence à l'élément parent
	var parentDiv = sp2.parentNode;

	// Insère le nouvel élément dans le DOM avant sp2
	parentDiv.insertBefore(sp1, sp2);
} */